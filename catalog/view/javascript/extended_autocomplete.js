// Autocomplete */
(function($) {
    $.fn.extended_autocomplete = function(option) {
        return this.each(function() {

            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('extended_autocomplete', 'off');

            // Focus
            $(this).on('focus', function() {
                this.request();
            });

            // Blur
            $(this).on('blur', function() {
                setTimeout(function(object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function(event) {
                switch(event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function(event) {
                event.preventDefault();

                value = $(event.target).closest('li').attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function() {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight() + 1,
                    left: pos.left-1
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function() {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function() {
                clearTimeout(this.timer);

                this.timer = setTimeout(function(object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function(json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '">';
                            if (json[i]['image']) {
                                html += '<a class="kep" href="#"><img src="' + json[i]['image'] + '"></a>';
                            }
                            html += '<a class="szoveg" href="#">';
                            html += '<span class="product_name">'+json[i]['label']+'</span>'//<span class="model"> '+json[i]['model']+'</span>';
                            html += '</a>';
                            html += '</li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
            $(this).siblings('ul.dropdown-menu').delegate('img', 'click', $.proxy(this.click, this));
            $(this).siblings('ul.dropdown-menu').delegate('span', 'click', $.proxy(this.click, this));
        });
    }
})(window.jQuery);
$(document).ready(function(){
    $("input[name='search']").val('');
    $("input[name='search2']").val('');
    $("input[name='search'], input[name='search2']").extended_autocomplete({
        'source': function(request, response) {
        var searchURL = 'index.php?route=product/search&search=' +  encodeURIComponent(request)+'&description=' +  encodeURIComponent(request)+'&autocomplete=1';
            console.log(searchURL);
            //debugger;
            $.ajax({
                url: searchURL,
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            image: item['thumb'],
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            url = $('base').attr('href') + 'index.php?route=product/product&product_id='+item.value;

            location = url;
            return false;
        }
    });
});
