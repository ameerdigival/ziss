<?php

// Heading
$_['heading_title']    = '<b>ZISS Home Page Slider</b>';

// Text
$_['text_success']     = 'Success: You have modified module ZISS Lite Revolution Slider!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module ZISS Lite Revolution Slider!';

?>